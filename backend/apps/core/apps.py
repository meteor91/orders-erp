from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'backend.apps.core'
