from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from collections import OrderedDict


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('total', self.page.paginator.count),
            ('has_next_page', True if self.get_next_link() else False),
            ('page', self.page.number),
            ('items', data),
            # lol, зачем это?
            ('page_size', self.page_size)
        ]))
