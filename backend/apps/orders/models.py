
from django.db import models

from core.models import BasicsModelMixin


class Order(BasicsModelMixin, models.Model):
    created_by = models.ForeignKey('users.User', related_name='+')
    executor = models.ForeignKey('users.User', related_name='+', null=True, blank=True)
    name = models.TextField('Название заказа', max_length=127, null=False, blank=False)
    latest_date = models.DateTimeField('Крайняя дата доставки')

    class Meta:
        ordering = '-created_at',
