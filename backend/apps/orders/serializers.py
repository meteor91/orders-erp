from rest_framework import serializers
from rest_framework_friendly_errors.mixins import FriendlyErrorMessagesMixin as FEMMixin

from users.serializers import UserPreviewSerializer
from .models import Order


class OrderSerializer(serializers.ModelSerializer):
    created_by = UserPreviewSerializer(read_only=True)

    # def validate_latest_date(self, value):
    #     raise serializers.ValidationError('date is wrong')
    #
    # def validate_name(self, value):
    #     raise serializers.ValidationError('name is wrong')


    class Meta:
        model = Order
        fields = 'id', 'created_by', 'executor', 'latest_date', 'name', 'created_at'
        read_only_fields = 'created_by',
