from rest_framework.routers import DefaultRouter

from .views import OrderView

router = DefaultRouter()
router.register(r'orders', OrderView)

urlpatterns = []

urlpatterns += router.urls