from rest_framework import viewsets, status
from rest_framework.response import Response

from .models import Order
from .serializers import OrderSerializer

import time

class OrderView(viewsets.ModelViewSet):
    """some docs"""
    serializer_class = OrderSerializer
    queryset = Order.objects

    def get_queryset(self):
        # user = self.request.user
        # time.sleep(1)
        return self.queryset.all()

    def perform_create(self, serializer):
        # time.sleep(8)
        serializer.save(created_by=self.request.user)

    def destroy(self, request, *args, **kwargs):
        time.sleep(3)
        instance = self.get_object()
        id = instance.id
        self.perform_destroy(instance)
        return Response({'id': id}, status=status.HTTP_200_OK)
