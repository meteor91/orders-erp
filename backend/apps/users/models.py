from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    class ROLES:
        DELIVERYMAN = 'deliveryman'
        MANAGER = 'manager'

    ROLES_CHOICES = (
        (ROLES.DELIVERYMAN, 'Доставщик'),
        (ROLES.MANAGER, 'Менеджер')
    )

    role = models.CharField('Роль', choices=ROLES_CHOICES, max_length=16)
