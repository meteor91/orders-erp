from rest_framework import serializers
from rest_framework_friendly_errors.mixins import FriendlyErrorMessagesMixin as FEMMixin

from users.models import User


class UserPreviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = 'id', 'username',
