import datetime
import os, sys
from configurations import Configuration

from backend.configs.DatabaseSettings import DatabaseSettings
from .configs.AuthValidatorsSettings import AuthValidatorSettings
from .configs.TemplatesSettings import TemplateSettings
from .configs.MiddlewareSettings import MiddlewareSettings
from .configs.RestFrameworkSettings import RestFrameworkSettings


class Base(DatabaseSettings, TemplateSettings, AuthValidatorSettings,
           MiddlewareSettings, RestFrameworkSettings, Configuration):

    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))
    sys.path.insert(0, os.path.join(PROJECT_ROOT, 'apps'))

    AUTH_USER_MODEL = 'users.User'
    ROOT_URLCONF = 'backend.urls'
    WSGI_APPLICATION = 'backend.wsgi.application'

    SITE_ID = 1

    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = '@o3(n92$f*bccgq(@muvk6ciitc)vt#&pi260wc*42!r5e7rn@'

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    ALLOWED_HOSTS = ['*']

    # try:
    #     pass
    # except KeyError:
    CORS_ORIGIN_ALLOW_ALL = True

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'corsheaders',
        'django.contrib.staticfiles',

        'rest_framework',
        'rest_framework_docs',
        'rest_framework_swagger',

        'core',
        'users',
        'orders',
    ]

    LANGUAGE_CODE = 'en-us'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True

    STATIC_URL = '/static/'

    @property
    def STATICFILES_DIRS(self):
        return (
            os.path.join(self.PROJECT_ROOT, 'staticfiles'),
        )

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )
    # @property
    # def BASE_DIR(self):
        # return self.PROJECT_ROOT


class Dev(Base):

    @property
    def MEDIA_ROOT(self):
        return os.path.join(self.PROJECT_ROOT, 'media')

    @property
    def STATIC_ROOT(self):
        return os.path.join(self.PROJECT_ROOT, 'static')


class Prod(Base):
    DEBUG = False
    ALLOWED_HOSTS = ['http://kkgenki.space/', 'kkgenki.space', 'www.kkgenki.space']

    @property
    def MEDIA_ROOT(self):
        return '/root/projects/orders-erp/backend/media'

    @property
    def STATIC_ROOT(self):
        return '/root/projects/orders-erp/backend/static'
