import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import {Router, Route, Switch } from 'react-router';
import {createBrowserHistory } from 'history';

import {IAppState} from 'core/models';
import {DefaultLayout} from 'core/layouts/DefaultLayout';
import {AuthRoute} from 'core/components/AuthRoute';
import {AppLoading} from 'core/components/AppLoading';
import {EProcessStatus} from 'core/Enums';

import {OrdersCreatePage } from 'modules/orders/pages/OrdersCreatePage';
import {OrdersEditPage} from 'modules/orders/pages/OrdersEditPage';
import {OrdersListPage} from 'modules/orders/pages/OrdersListPage';
import {VerifyTokenAction} from 'modules/users/Actions';
import {LoginPage} from 'modules/users/pages/LoginPage';

export const history = createBrowserHistory();

interface IStateProps {
    token: string;
    authenticated: boolean;
    verifyStatus: EProcessStatus;
}

interface IDispatchProps {
    verifyToken: (token: string) => void;
}

type IProps = IStateProps & IDispatchProps;

function AuthArea() {
    return (
        <DefaultLayout>
            <Route path="/app/orders/create" exact={true} strict={true} component={OrdersCreatePage} />
            <Route path="/app/orders/:id/edit" exact={true} strict={true} component={OrdersEditPage} />
            <Route path="/app/orders" exact={true} strict={true} component={OrdersListPage}/>
        </DefaultLayout>
    );
}

class App extends React.Component<IProps, {}> {
    componentWillMount() {
        const {verifyToken, token} = this.props;
        verifyToken(token);
    }

    render() {
        const {verifyStatus} = this.props;
        return (
            <Router history={history}>
                {verifyStatus === EProcessStatus.RUNNING ?
                    <AppLoading/> :
                    <Switch>
                        <Route path="/login/" component={LoginPage} />

                        <AuthRoute path="/app/" authenticated={this.props.authenticated} component={AuthArea}/>
                    </Switch>
                }
            </Router>
        );
    }
}

const mapStateToProps = (state: IAppState) => ({
    token: state.users.login.token,
    authenticated: state.users.login.authenticated,
    verifyStatus: state.users.login.verifyStatus,
});

const mapDispatchToProps = (dispatch: Dispatch<IAppState>) => ({
    verifyToken: (token: string) => VerifyTokenAction(dispatch, token),
});

const AppConnected = connect<IStateProps, IDispatchProps, {}>(mapStateToProps, mapDispatchToProps)(App);

export {AppConnected as App};
