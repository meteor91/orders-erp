export enum EProcessStatus {
    IDLE = 'IDLE',
    RUNNING = 'RUNNING',
    SUCCESS = 'SUCCESS',
    FAIL = 'FAIL',
    CANCELED = 'CANCELED',
}

export enum EFormFieldType {
    DateTime = 'DateTime',
}
