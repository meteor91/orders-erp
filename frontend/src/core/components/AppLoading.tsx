import * as React from 'react';
import {Layout, Row, Spin} from 'antd';

const styles = {
    layout: {height: '100vh'},
};

export function AppLoading() {
    return (
        <Layout style={styles.layout}>
            <Row type="flex" justify="space-around" align="middle" style={styles.layout}>
                <Spin size="large"/>
            </Row>
        </Layout>
    );
}
