import * as React from 'react';
import {Route, Redirect} from 'react-router';

export const AuthRoute = ({ component: Component, authenticated, ...rest }) => (
    <Route
        {...rest}
        render={props => (
            authenticated ? (
                <Component {...props}/>
            ) : (
                <Redirect to={{pathname: '/login', state: { from: props.location }}}/>
            )
        )}
    />
);
