import * as React from 'react';
import {Spin} from 'antd';

import {EProcessStatus} from '../Enums';

interface IProps {
    status?: EProcessStatus;
    isLoading?: boolean;
}

export function ContentLoadingOverlay(props: IProps) {
    return props.status === EProcessStatus.RUNNING || props.isLoading ?
        <Spin size="large" className="content-overlay"/> :
        null;
}
