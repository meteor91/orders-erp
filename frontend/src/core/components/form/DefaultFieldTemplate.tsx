import * as React from 'react';

export function MyDefaultFieldTemplate(props: any) {
    const {id, label, /* classNames, help, description*/ required, rawErrors, children} = props;
    if (id === 'root') {
        return children;
    }

    return (
        <div className={`ant-row ant-form-item ${rawErrors && 'ant-form-item-with-help'}`}>
            <div className="ant-col-5 ant-form-item-label">
                <label className="ant-form-item-required" title={label}>{label}</label>
            </div>
            <div className="ant-col-12 ant-form-item-control-wrapper">
                <div className={`ant-form-item-control ${rawErrors && 'has-error'}`}>
                    <span className="ant-form-item-children">
                        {children}
                    </span>
                    {rawErrors && <div className="ant-form-explain">{rawErrors[0]}</div>}
                </div>
            </div>
        </div>
    );
}

export function MyObjectFieldTemplate(props: any) {
    return (
        <div>
            {/*{props.title}*/}
            {/*{props.description}*/}
            {props.properties.map(element => <div key={element.content.key}>{element.content}</div>)}
        </div>
    );
}
