import {isEmpty} from 'lodash';
import * as React from 'react';
import Form, {FormProps} from 'react-jsonschema-form';
import {buildServerErrors} from '../../utils/buildServerErrors';

interface IEnhancedFormProps<T> extends FormProps<T> {
    serverErrors?: any;
}

class EnhancedForm extends Form<any, IEnhancedFormProps<any>> {
    getStateFromProps(props: IEnhancedFormProps<any>) {
        const state = super.getStateFromProps(props);

        if (!isEmpty(props.serverErrors) && props.serverErrors !== this.props.serverErrors) {
            state.errorSchema = buildServerErrors(props.serverErrors);
        }

        return state;
    }
}

export {EnhancedForm as Form};
