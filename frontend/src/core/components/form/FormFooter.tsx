import * as React from 'react';
import {get} from 'lodash';
import {FormItem} from 'core/components/form/FormItem';

interface IProps {
    children?: any;
    errors: any;
    submitLabel: string;
}

export function FormFooter(props: IProps) {
    const {children, submitLabel, errors} = props;
    return (
        <FormItem>
            <div className={`ant-form-item-control ${errors && 'has-error'}`}>
                {children}
                <button type="submit" className="ant-btn login-form-button ant-btn-primary ant-btn-lg">
                    <span>{submitLabel}</span>
                </button>
                {
                    get(errors, 'nonFieldErrors') &&
                    errors.nonFieldErrors.map(
                        message => (<div key={message} className="ant-form-explain">{message}</div>),
                    )
                }
            </div>
        </FormItem>
    );
}
