import * as React from 'react';
import * as moment from 'moment';
import {DatePicker as AntDatePicker} from 'antd';
import {WidgetProps} from 'react-jsonschema-form';

type Moment = moment.Moment;

const dateFormat = 'YYYY-MM-DD HH:mm:ss';

const DatePicker = (props: WidgetProps) => {
    const value = props.value ?
        moment(props.value, dateFormat) :
        null;
    return (
        <AntDatePicker
            format={dateFormat}
            defaultValue={null}
            value={value}
            showTime={true}
            onChange={(date: Moment, _dateString: string) => {
                if (date) {
                    props.onChange(date.format(dateFormat));
                } else {
                    props.onChange('');
                }
            }}
        />
    );
};

export {DatePicker};
