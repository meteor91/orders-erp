import * as React from 'react';
import {Input} from 'antd';
import {WidgetProps} from 'react-jsonschema-form';

const TextInput = (props: WidgetProps) => {
    const options = props.options || {};
    return (
        <Input
            value={props.value}
            onChange={(value) => props.onChange(value.target.value)}
            placeholder={props.placeholder}
            {...options}
        />
    );
};

export {TextInput};
