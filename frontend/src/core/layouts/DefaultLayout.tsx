import * as React from 'react';
import {Layout, Menu, Icon} from 'antd';
const {Header, Sider, Content} = Layout;

const styles = {
    layout: {height: '100vh'},
    content: { margin: '24px 4px', padding: 24, background: '#fff', minHeight: 280 },
};

export class DefaultLayout extends React.Component {
    state = {
        collapsed: false,
    };
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    onMenuClick = ({ item, key, keyPath }) => {
        console.log(item, key, keyPath);
    }

    render() {
        return (
            <Layout style={styles.layout}>
                <Sider
                    trigger={null}
                    collapsible={true}
                    collapsed={this.state.collapsed}
                >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} onClick={this.onMenuClick}>
                        <Menu.Item key="1">
                            <Icon type="car" />
                            <span>Заказы</span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span>nav 2</span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <span>nav 3</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                    </Header>
                    <Content style={styles.content}>
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        );
    }
}
