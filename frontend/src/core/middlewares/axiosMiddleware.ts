import axios from 'axios';
import axiosMiddleware, {getActionTypes} from 'redux-axios-middleware';
import {camelCase, reduce} from 'lodash';
import {adaptFromApi, adaptToApi} from '../utils/caseAdapter';
import {IServerCheckError} from '../models';

const client = axios.create({
    baseURL: 'http://localhost:8000/api',
    responseType: 'json',
});

// допилить настройки
// onSuccess/onError({ action: action, next: next, response: response, getState: getState, dispatch: dispatch}, options)
const axiosMiddlewareOptions = {
    interceptors: {
        request: [
            (store, config) => {
                const state = store.getState();
                // debugger;
                if (state.users && state.users.login.token) {
                    config.headers.Authorization = 'JWT ' + state.users.login.token;
                    console.log(config.headers.Authorization);
                }
                config.data = adaptToApi(config.data);
                return config;
            },
        ],
    },
    returnRejectedPromiseOnError: true,
    onSuccess: ({ action, next, response }, options) => {
        const nextAction = {
            type: getActionTypes(action, options)[1],
            payload: adaptFromApi(response.data),
            meta: {
                previousAction: action,
            },
        };

        return next(nextAction);
    },
    // разобраться со структурой ответа
    onError: ({ action, next, error }, options) => {
        let errorObject;
        if (!error.response) {
            errorObject = {
                data: error.message,
                status: 0,
            };
            if (process.env.NODE_ENV !== 'production') {
                console.log('HTTP Failure in Axios', error);
            }
        } else {
            if (error.response.data.errors) {
                error.response.data.checks =
                    reduce(error.response.data.errors, (result, item: IServerCheckError) => {
                        result[camelCase(item.field)] = {code: item.code, message: item.message};
                        return result;
                    }, {});

                delete error.response.data.errors;
            }
            errorObject = {errors: error.response.data};
        }
        const nextAction = {
            type: getActionTypes(action, options)[2],
            payload: adaptFromApi(errorObject),
            meta: {
                previousAction: action,
            },
        };

        return next(nextAction);
    },
};

export default axiosMiddleware(client, axiosMiddlewareOptions);
