export interface ICreateActions<T> {
    create: (data: T) => Promise<T>;
    clearServerValidation: () => void;
}

export interface IUpdateActions<T> {
    update: (data: T) => Promise<T>;
    clearServerValidation: () => void;
    clearDetails: () => void;
}

export interface IRemoveActions {
    remove: (id: string) => Promise<string>;
}

export interface ILoadItemActions<T> {
    getById: (id: string) => Promise<T>;
}

export interface ILoadListActions<T> {
    getList: (requestParams: any) => Promise<T>;
}

export interface IEntireActions<T> extends
    ICreateActions<T>,
    IUpdateActions<T>,
    IRemoveActions,
    ILoadItemActions<T>,
    ILoadListActions<T> {
}
