import {IUsersState} from 'modules/users/models';
import {IOrdersReduxState} from 'modules/orders/models';

import {EFormFieldType, EProcessStatus} from '../Enums';
// todo возможно переименовать на IServerValidationErrors
export interface IValidationErrors {
    nonFieldErrors: string[];
}

export interface IAppState {
    users: IUsersState;
    orders: IOrdersReduxState;
}

export interface IAsyncData<T> {
    status: EProcessStatus;
    data: T;
    errors: IErrorResponse;
}

export interface IServerCheckError {
    message: string;
    code: string;
    field: string;
}

export interface ICheckErrors {
    [field: string]: {
        message: string;
        code: string;
    }
}

export interface IErrorResponse {
    message: string;
    code: string;
    checks?: ICheckErrors;
}

export interface IFormFieldSettings {
    id: string;
    type: EFormFieldType;
    label?: string;
}

export interface IFormProps<T> {
    details: IAsyncData<T>;
}

export interface IPaginatedList<T> {
    hasNextPage: boolean;
    page: number;
    items: T[];
    total: number;
    pageSize: number;
}

// interface IAsyncList<T> {
//     status: EProcessStatus;
//     data: IAsyncData<IPaginatedList<T>>;
//     errors: IErrorResponse;
// }

// export interface IFilteredList<T> extends IPaginatedList<T> {
//     // TODO запилить стандартный интерфейс фильтров
//     filter?: any;
// }

// export interface IAsyncData<T> {
//     status: EProcessStatus;
//     data: T;
//     errors: IErrorResponse;
// }
