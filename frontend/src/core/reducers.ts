import {combineReducers} from 'redux';

import {users } from 'modules/users/reducers';
import {orders} from 'modules/orders/reducers';
import {IAppState} from './models';

export default combineReducers<IAppState>({
    users,
    orders,
});
