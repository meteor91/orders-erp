import {createStore, applyMiddleware, Store} from 'redux';
import loggerMiddleware from './middlewares/loggerMiddleware';
import axiosMiddleware from './middlewares/axiosMiddleware';
import rootReducer from './reducers';
import {IAppState} from './models';

import {initial as usersInitial} from 'modules/users/reducers';
import {initial as ordersInitial} from 'modules/orders/reducers';

function getInitialState(): IAppState {
    const initialState: IAppState = {
        users: usersInitial.state,
        orders: ordersInitial.state,
    };

    return initialState;
}

export function configureStore(): Store<IAppState> {
    const initialState: IAppState = getInitialState();
    const create = window.devToolsExtension
        ? window.devToolsExtension()(createStore)
        : createStore;

    const createStoreWithMiddleware = applyMiddleware(
        loggerMiddleware,
        axiosMiddleware,
    )(create);

    const store = createStoreWithMiddleware(rootReducer, initialState) as Store<IAppState>;

    if (module.hot) {
        module.hot.accept('./reducers', () => {
            const nextReducer = require('./reducers');
            store.replaceReducer(nextReducer);
        });
    }

    return store;
}
