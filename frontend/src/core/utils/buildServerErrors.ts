export function buildServerErrors(errors: any) {
    const result = {};
    for (const key in errors) {
        result[key] = {__errors: errors[key]};
    }
    return result;
}
