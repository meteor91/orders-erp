import {camelCase, forEach, isArray, isObject, map, snakeCase} from 'lodash';

function adapt(sourceObject: any, adaptFunction: (str: string | number) => string) {
    if (sourceObject && sourceObject.constructor === File) {
        // Файлы
        return sourceObject;
    } else if (isArray(sourceObject)) {
        return map(sourceObject, value => adapt(value, adaptFunction));
    } else if (isObject(sourceObject)) {
        const adaptedObject = {};
        forEach(sourceObject, (value, key) => {
            const adaptedKey = adaptFunction(key);
            if (isObject(value) === true || isArray(value) === true) {
                adaptedObject[adaptedKey] = adapt(value, adaptFunction);
            } else {
                adaptedObject[adaptedKey] = value;
            }
        });
        return adaptedObject;
    } else {
        // strings, numbers, etc.
        return sourceObject;
    }
}

export const adaptFromApi = sourceObject => adapt(sourceObject, camelCase);

export const adaptToApi = sourceObject => adapt(sourceObject, snakeCase);
