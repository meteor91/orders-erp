import {Dispatch} from 'redux';
import {IAppState} from 'core/models';

export function dispatchAsync(dispatch: Dispatch<IAppState>, action: any): Promise<any> {
    return dispatch(action) as Promise<any>;
}
