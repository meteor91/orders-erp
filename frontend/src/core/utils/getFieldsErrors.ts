import {forOwn} from 'lodash';
import {IErrorResponse} from '../models';

export function getFieldsErrors(errors: IErrorResponse) {
    const result = {};
    // debugger
    if (errors) {
        forOwn(errors.checks, (check, fieldName) => {
            result[fieldName] =  {
                help: check.message,
                validateStatus: 'error',
            };
        });
    }

    return result;
}
