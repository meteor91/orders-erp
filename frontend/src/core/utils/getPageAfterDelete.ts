import {IPaginatedList} from '../models';

export function getPageAfterDelete(paginatedList: IPaginatedList<any>) {
    if ((paginatedList.page - 1) * paginatedList.pageSize === paginatedList.total - 1) {
        return paginatedList.page - 1 || 1;
    } else {
        return paginatedList.page;
    }
}
