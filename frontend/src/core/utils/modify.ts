import * as dotProp from 'dot-prop-immutable';

export function set<T>(state: T, path: string, value: any): T {
    return dotProp.set<T>(state, path, value);
}

export default function<T>(state: T) {
    return {
        set: (path: string, value: any) => dotProp.set(state, path, value),
        remove: (path: string) => dotProp.delete(state, path),
    };
}
