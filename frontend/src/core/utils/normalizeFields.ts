import * as moment from 'moment';
import {forOwn} from 'lodash';
import {EFormFieldType} from '../Enums';
import {IFormFieldSettings} from '../models/index';

// todo доработать все

export function normalizeFields<T>(values: T): T {
    const result = {};
    forOwn(values, (value, key) => {
        if (value instanceof moment) {
            result[key] = value.format('YYYY-MM-DD HH:mm:ss');
        } else {
            result[key] = value;
        }
    });
    return result as T;
}

export function valueToFormField(field: IFormFieldSettings, value: any) {
    if (field.type === EFormFieldType.DateTime) {
        return moment(value);
    } else {
        return value;
    }
}
