import {Dispatch} from 'redux';
import {IAppState} from '../models';

export type Dispatch = Dispatch<IAppState>;
