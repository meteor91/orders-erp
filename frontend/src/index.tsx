import * as BluebirdPromise from 'bluebird';

global.Promise = BluebirdPromise;
BluebirdPromise.config({
    longStackTraces: true,
    warnings: true, // note, run node with --trace-warnings to see full stack traces for warnings
});

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import {LocaleProvider} from 'antd';
import * as ruRU from 'antd/lib/locale-provider/ru_RU';

import 'antd/dist/antd.less';
import './styles/styles.less';

import {configureStore} from './core/store';

import {App} from './App';

const store = configureStore();

ReactDOM.render(
    <LocaleProvider locale={ruRU as any}>
        <Provider store={store}>
            <App/>
        </Provider>
    </LocaleProvider>,
    document.getElementById('root'),
);
