import {Dispatch} from 'core/utils/redux';
import {dispatchAsync} from 'core/utils/dispatchAsync';
import {IEntireActions} from 'core/models/actions';
import {IOrder} from './models';


export class OrdersActions implements IEntireActions<IOrder> {
    constructor(private dispatch: Dispatch) {};

    getById = (id: string): Promise<any> => {
        return dispatchAsync(
            this.dispatch,
            {
                type: 'orders/orders/loadDetails',
                payload: {
                    request: {
                        url: `/orders/orders/${id}/`,
                        method: 'get',
                    },
                },
            },
        );
    }

    create = (data: IOrder): Promise<IOrder> => {
        return dispatchAsync(
            this.dispatch,
            {
                type: 'orders/orders/create',
                payload: {
                    request: {
                        url: '/orders/orders/',
                        method: 'post',
                        data,
                    },
                },
            },
        );
    }

    update = (data: IOrder): Promise<IOrder> => {
        return dispatchAsync(
            this.dispatch,
            {
                type: 'orders/orders/update',
                payload: {
                    request: {
                        url: `/orders/orders/${data.id}/`,
                        method: 'put',
                        data,
                    },
                },
            },
        );
    }

    clearServerValidation = () => this.dispatch({type: 'orders/orders/clearServerValidation'});

    clearDetails = () => this.dispatch({type: 'orders/orders/clearDetails'});

    remove = (id: string): Promise<any> => {
        return dispatchAsync(
            this.dispatch,
            {
                type: 'orders/orders/delete',
                payload: {
                    request: {
                        url: `/orders/orders/${id}/`,
                        method: 'delete',
                    },
                    id,
                },
            },
        );
    }

    getList = (requestParams: any) => {
        return dispatchAsync(
            this.dispatch,
            {
                type: 'orders/orders/list',
                payload: {
                    request: {
                        url: '/orders/orders/',
                        method: 'get',
                        params: requestParams,
                    },
                },
            },
        );
    }
}
