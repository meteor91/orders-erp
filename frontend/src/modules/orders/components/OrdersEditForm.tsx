import * as React from 'react';
import {Dispatch, connect} from 'react-redux';
import {UiSchema} from 'react-jsonschema-form';
import {JSONSchema6} from 'json-schema';
import {EProcessStatus} from 'core/Enums';
import {IAppState, IAsyncData} from 'core/models';
import {ContentLoadingOverlay} from 'core/components/ContentLoadingOverlay';
import {Form} from 'core/components/form/Form';
import {MyDefaultFieldTemplate, MyObjectFieldTemplate} from 'core/components/form/DefaultFieldTemplate';
import {DatePicker, TextInput} from 'core/components/form/widgets';
import {FormFooter} from 'core/components/form/FormFooter';
import {OrdersActions} from '../Actions';
import {IOrder} from '../models';

interface IStateProps {
    details: IAsyncData<IOrder>;
    errors?: any;
}

interface IDispatchProps {
    actions: OrdersActions;
}

interface IState {
    submitStatus: EProcessStatus;
}

interface IProps extends IStateProps, IDispatchProps {
    id?: string;
    onSuccess: () => void;
}

const schema: JSONSchema6 = {
    type: 'object',
    required: ['latestDate', 'name'],
    properties: {
        latestDate: {type: 'string', title: 'Доставить до'},
        name: {type: 'string', title: 'Название'},
    },
};

const uiSchema: UiSchema = {
    latestDate: {
        'ui:widget': DatePicker,
    },
    name: {
        'ui:widget': TextInput,
    },
};

class OrdersEditForm extends React.Component<IProps, IState> {
    state = {
        submitStatus: EProcessStatus.IDLE,
    };

    componentDidMount() {
        const {actions, id} = this.props;
        if (id) {
            actions.getById(id);
        }
    }

    componentWillUnmount() {
        this.props.actions.clearDetails();
    }

    onSubmit = (formResult) => {
        const {actions, id} = this.props;
        (id ? actions.update : actions.create)(formResult.formData).then(this.props.onSuccess);
    }

    render() {
        const {details, errors} = this.props;

        return (
            <div>
                <ContentLoadingOverlay status={details.status}/>
                <Form
                    className="ant-form ant-form-horizontal"
                    showErrorList={false}
                    onSubmit={this.onSubmit}
                    formData={details.data}
                    serverErrors={details.errors}
                    schema={schema}
                    uiSchema={uiSchema}
                    FieldTemplate={MyDefaultFieldTemplate}
                    ObjectFieldTemplate={MyObjectFieldTemplate}
                >
                    <FormFooter submitLabel="Отправить" errors={errors}/>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state: IAppState): IStateProps => {
    const details = state.orders.orders.details;
    return {
        details,
    };
};

const mapDispatchToProps = (dispatch: Dispatch<any>): IDispatchProps  => ({
    actions: new OrdersActions(dispatch),
});

const OrdersEditFormConnected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrdersEditForm);

export {OrdersEditFormConnected as OrdersEditForm};
