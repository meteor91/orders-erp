import * as React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Icon, Table} from 'antd';
const {Column} = Table;
// import moment = require('moment');
import * as moment from 'moment';

import {EProcessStatus} from 'core/Enums';
import {IAppState, IAsyncData, IPaginatedList} from 'core/models';
import {getPageAfterDelete} from 'core/utils/getPageAfterDelete';
import {OrdersActions} from '../Actions';
import {IOrder} from '../models';

interface IStateProps {
    list: IAsyncData<IPaginatedList<IOrder>>;
    remove: {
        [key: number]: IAsyncData<number>,
    };
}

interface IDispatchProps {
    action: OrdersActions;
}

type IProps = IStateProps & IDispatchProps;

class OrdersList extends React.Component<IProps, {}> {
    componentWillMount() {
        this.props.action.getList({page: 1});
    }

    onChange = (page: number, pageSize: number) => {
        this.props.action.getList({page});
    }

    onDelete = (id) => {
        const {action, list} = this.props;
        action.remove(id)
            .then(() => action.getList({page: getPageAfterDelete(list.data)}));
    }

    renderDate = (text, record) => moment.utc(record.latestDate).format('ll');

    renderDeleteAction = (text, order: IOrder) => {
        const remove = this.props.remove[order.id] && this.props.remove[order.id].status === EProcessStatus.RUNNING;
        return (
            <div>
                <Link to={`/app/orders/${order.id}/edit`}>Edit</Link>
                <span className="ant-divider"/>
                <a href="#" onClick={(e) => e.preventDefault() || this.onDelete(order.id)}>
                    Delete {remove && <Icon type="loading"/>}
                </a>
            </div>
        );
    }

    renderEditAction = (text, order: IOrder) => (
        <Link to={`app/orders/${order.id}/edit`}>Редактировать</Link>
    )

    render() {
        const loading = this.props.list.status === EProcessStatus.RUNNING;
        const {total, page, pageSize, items} = this.props.list.data;
        const pagination = {
            current: page,
            total, pageSize,
            onChange: this.onChange,
        };
        return (
            <div>
                <Table
                    size="middle"
                    dataSource={items}
                    pagination={pagination}
                    loading={loading}
                >
                    <Column
                        title="name"
                        dataIndex="name"
                        key="name"
                    />
                    <Column
                        title="latestDate"
                        dataIndex="latestDate"
                        key="latestDate"
                        render={this.renderDate}
                    />
                    <Column
                        title="createdBy"
                        dataIndex="createdBy.username"
                        key="createdBy"
                    />
                    <Column
                        title="Action"
                        key="action"
                        render={this.renderDeleteAction}
                    />
                </Table>
            </div>
        );
    }
}

const mapStateToProps = (state: IAppState) => ({
    list: state.orders.orders.list,
    remove: state.orders.orders.remove,
});

const mapDispatchToProps = (dispatch) => ({
    action: new OrdersActions(dispatch),
});

const OrdersListConnected = connect<IStateProps, IDispatchProps, {}>(
    mapStateToProps,
    mapDispatchToProps,
)(OrdersList);

export {OrdersListConnected as OrdersList};
