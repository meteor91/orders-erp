import {EFormFieldType} from 'core/Enums';
import {IFormFieldSettings} from 'core/models';

export const OrdersFormFields: IFormFieldSettings[] = [
    {
        id: 'latestDate',
        type: EFormFieldType.DateTime,
        label: 'Доставить до',
    },
];
