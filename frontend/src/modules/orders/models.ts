import {IAsyncData, IPaginatedList} from 'core/models';
import {IUser} from 'modules/users/Models';

export interface IOrder {
    id?: string;
    latestDate: string;
    executor?: string;
    createdBy: IUser;
}

export interface IOrdersState {
    details: IAsyncData<IOrder>;
    list: IAsyncData<IPaginatedList<IOrder>>;
    remove: {
        [key: number]: IAsyncData<number>,
    }
}

export interface IOrdersReduxState {
    orders: IOrdersState;
}
