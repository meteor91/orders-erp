import * as React from 'react';
import {Link} from 'react-router-dom';

import {history} from 'App';
import {OrdersEditForm} from '../components/OrdersEditForm';

export class OrdersCreatePage extends React.Component<{}, {}> {

    onSuccess = () => history.push('/app/orders');

    render() {
        return (
            <div className="box">
                <Link to={`/app/orders`}>Назад</Link>
                <OrdersEditForm onSuccess={this.onSuccess}/>
            </div>
        );
    }
}
