import * as React from 'react';
import {Link} from 'react-router-dom';
import {RouteComponentProps} from 'react-router';

import {history} from 'App';
import {OrdersEditForm} from '../components/OrdersEditForm';

interface IProps extends RouteComponentProps<{id: string}> {

}

export class OrdersEditPage extends React.Component<IProps, {}> {
    onSuccess = () => history.push('/app/orders');

    render() {
        return (
            <div className="box">
                <Link to={`/app/orders`}>Назад</Link>
                <OrdersEditForm
                    id={this.props.match.params.id}
                    onSuccess={this.onSuccess}
                />
            </div>
        );
    }
}
