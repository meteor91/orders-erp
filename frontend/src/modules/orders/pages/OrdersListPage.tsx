import * as React from 'react';
import {Link} from 'react-router-dom';
import {Col, Row} from 'antd';

import {OrdersList} from '../components/OrdersList';

export class OrdersListPage extends React.Component<{}, {}> {

    render() {
        return (
            <div>
                <Row>
                    <Col span={24}>
                        <Link to="/app/orders/create" className="ant-btn ant-btn-primary">
                            <span>Новый заказ</span>
                        </Link>
                    </Col>
                </Row>
                <OrdersList/>
            </div>
        );
    }
}
