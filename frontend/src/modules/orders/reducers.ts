import {Action} from 'redux-actions';
import modify from 'core/utils/modify';
import {EProcessStatus} from 'core/Enums';

import {IOrder, IOrdersReduxState} from './models';

export const initial = {
    get state(): IOrdersReduxState {
        return {
            orders: {
                details: {
                    errors: null,
                    status: EProcessStatus.IDLE,
                    data: {
                        latestDate: null,
                        createdBy: null,
                    },
                },
                list: {
                    errors: null,
                    status: EProcessStatus.IDLE,
                    data: {
                        items: null,
                        page: 1,
                        pageSize: 10,
                        total: 0,
                        hasNextPage: false,
                    },
                },
                remove: {},
            },
        };
    },
};

export function orders(state: IOrdersReduxState = initial.state, action: Action<any>): IOrdersReduxState {
    const payload = action.payload;
    let index;

    switch (action.type) {
        case 'orders/orders/create':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                // todo сделать по нормальному
                ...state.orders.details,
                status: EProcessStatus.RUNNING,
            });
        case 'orders/orders/create_SUCCESS':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                data: payload,
                status: EProcessStatus.SUCCESS,
            });

        case 'orders/orders/create_FAIL':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                errors: payload.errors,
                status: EProcessStatus.FAIL,
            });
        case 'orders/orders/update_FAIL':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                errors: payload.errors,
                status: EProcessStatus.FAIL,
            });

        case 'orders/orders/loadDetails':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                ...state.orders.details,
                data: null,
                status: EProcessStatus.RUNNING,
            });
        case 'orders/orders/loadDetails_SUCCESS':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                data: payload,
                status: EProcessStatus.SUCCESS,
            });

        case 'orders/orders/loadDetails_FAIL':
            return modify<IOrdersReduxState>(state).set('orders.details', {
                errors: payload.errors,
                status: EProcessStatus.FAIL,
            });

        case 'orders/orders/clearServerValidation':
            return modify<IOrdersReduxState>(state).set('orders.details.errors', null);
        case 'orders/orders/clearDetails':
            return modify<IOrdersReduxState>(state).set('orders.details', initial.state.orders.details);
        case 'orders/orders/list':
            return modify<IOrdersReduxState>(state).set('orders.list', {
                // todo сделать по нормальному
                ...state.orders.list,
                status: EProcessStatus.RUNNING,
            });
        case 'orders/orders/list_SUCCESS':
            return modify<IOrdersReduxState>(state).set('orders.list', {
                data: payload,
                status: EProcessStatus.SUCCESS,
            });

        case 'orders/orders/list_FAIL':
            return modify<IOrdersReduxState>(state).set('orders.list', {
                errors: payload.errors,
                status: EProcessStatus.FAIL,
                data: payload,
            });

        case 'orders/orders/delete':
            // index = state.orders.list.data.items.findIndex((order: IOrder) => order.id === payload.id);
            return modify<IOrdersReduxState>(state).set(
                `orders.remove.${payload.id}`,
                {status: EProcessStatus.RUNNING, id: payload.id},
            );
        case 'orders/orders/delete_SUCCESS':
            index = state.orders.list.data.items.findIndex((order: IOrder) => order.id === payload.id);
            state = modify<IOrdersReduxState>(state).remove(
                `orders.list.data.items.${index}`,
            );
            return modify<IOrdersReduxState>(state).remove(
                `orders.remove.${payload.id}`,
            );

        case 'orders/orders/list/process_FAIL':
            index = state.orders.list.data.items.findIndex((order: IOrder) => order.id === payload.id);
            return modify<IOrdersReduxState>(state).set(
                `orders.remove.${payload.id}`,
                {status: EProcessStatus.FAIL, id: payload.id, errors: payload.errors}
            );

    }
    return state;
}
