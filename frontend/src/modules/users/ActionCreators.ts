export function login(data: any) {
    return {
        type: 'users/login',
        payload: {
            request: {
                url: '/token/auth/',
                method: 'post',
                data,
            },
        },
    };
}

export function verifyToken(token: string) {
    return {
        type: 'users/token/verify',
        payload: {
            request: {
                url: '/token/verify/',
                method: 'post',
                data: {token},
            },
        },
    };
}
