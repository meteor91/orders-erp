import { history } from 'App';

import {login, verifyToken} from './ActionCreators';
import {Dispatch} from 'redux';
import {IAppState} from 'core/models';
import {dispatchAsync} from 'core/utils/dispatchAsync';

export function LoginAction(dispatch: Dispatch<IAppState>, formData: any) {
    dispatchAsync(dispatch, login(formData)).then(
        ({payload}) => {
            window.localStorage.setItem('token', payload.token);
            history.push('/app/orders');
        },
    );
}

export function VerifyTokenAction(dispatch: Dispatch<IAppState>, token: string) {
    if (!token) {
        history.push('/login/');
    } else {
        dispatchAsync(dispatch, verifyToken(token)).then(
            () => {
                history.push('/app/orders');
            },
            ({payload}) => {
                localStorage.removeItem('token');
                history.push('/login/');
            },
        );
    }
}
