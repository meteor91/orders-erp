import * as React from 'react';

export function LoginFieldTemplate(props: any) {
    const {id, required, rawErrors, children} = props;
    if (id === 'root') {
        return children;
    }
    return (
        <div className="ant-row ant-form-item">
            <div className="ant-form-item-control-wrapper">
                <div className={`ant-form-item-control ${rawErrors ? 'has-error' : ''}`}>
                    <span className="ant-input-affix-wrapper">
                        {children}
                    </span>
                    {
                        rawErrors &&
                        <div className="ant-form-explain">{rawErrors[0]}</div>
                    }
                </div>
            </div>
        </div>
    );
}

export function LoginObjectFieldTemplate(props: any) {
    return (
        <div>
            {/*{props.title}*/}
            {/*{props.description}*/}
            {props.properties.map(element => <div key={element.content.key}>{element.content}</div>)}
        </div>
    );
}
