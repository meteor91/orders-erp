import {Icon} from 'antd';
import * as React from 'react';
import {Dispatch, connect} from 'react-redux';
import {UiSchema} from 'react-jsonschema-form';
import {get} from 'lodash';
import {JSONSchema6} from 'json-schema';

import {EProcessStatus} from 'core/Enums';
import {IAppState} from 'core/models';
import {Form} from 'core/components/form/Form';
import {FormItem} from 'core/components/form/FormItem';
import {TextInput} from 'core/components/form/widgets/TextInput';
import {MyObjectFieldTemplate} from 'core/components/form/DefaultFieldTemplate';
import {LoginFieldTemplate} from 'modules/users/components/LoginFieldTemplate';
import {LoginAction} from '../Actions';

const schema: JSONSchema6 = {
    type: 'object',
    required: ['username', 'password'],
    properties: {
        username: {type: 'string'},
        password: {type: 'string'},
    },
};

const uiSchema: UiSchema = {
    username: {
        'ui:widget': TextInput,
        'ui:options': {
            prefix: <Icon type="user" style={{ fontSize: 13 }} />,
            placeholder: 'Логин',
            size: 'large',
        },
    },
    password: {
        'ui:widget': TextInput,
        'ui:options': {
            prefix: <Icon type="lock" style={{ fontSize: 13 }} />,
            type: 'password',
            placeholder: 'Пароль',
            size: 'large',
        },
    },
};

interface IStateProps {
    errors: any;
    status: EProcessStatus;
}

interface IDispatchProps {
    login: (formData: any) => void;
}

interface IState {
    form: {
        username: string;
        password: string;
    }
}

type IProps = IStateProps & IDispatchProps;

class LoginForm extends React.Component<IProps, IState> {
    state = {
        form: {
            username: '',
            password: '',
        },
    };

    onSubmit = (formResult) => {
        this.setState({form: formResult.formData});
        this.props.login(formResult.formData);
    }

    render() {
        const {errors} = this.props;
        return (
            <Form
                onSubmit={this.onSubmit}
                showErrorList={false}
                className="ant-form ant-form-horizontal login-form"
                schema={schema}
                uiSchema={uiSchema}
                formData={this.state.form}
                serverErrors={this.props.errors}
                FieldTemplate={LoginFieldTemplate}
                ObjectFieldTemplate={MyObjectFieldTemplate}
            >
                <FormItem>
                    <div className={`ant-form-item-control ${errors && 'has-error'}`}>
                        <button type="submit" className="ant-btn login-form-button ant-btn-primary ant-btn-lg">
                            <span>Log in</span>
                        </button>
                        {
                            get(errors, 'nonFieldErrors') &&
                            errors.nonFieldErrors.map(
                                message => (<div key={message} className="ant-form-explain">{message}</div>)
                            )
                        }
                    </div>
                </FormItem>
            </Form>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch<IAppState>) => ({
    login: (formData) => LoginAction(dispatch, formData),
});

const mapStateToProps = (state: IAppState) => ({
    errors: state.users.login.errors,
    status: state.users.login.status,
});

const LoginFormConnected = connect<IStateProps, IDispatchProps, {}>(
    mapStateToProps,
    mapDispatchToProps,
)(LoginForm);

export {LoginFormConnected as LoginForm};
