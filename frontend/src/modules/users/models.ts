import {IValidationErrors} from 'core/models';
import {EProcessStatus} from 'core/Enums';

export interface IUser {
    id: string;
    username: string;

}

export interface ILoginState {
    errors?: IValidationErrors;
    status: EProcessStatus;
    verifyStatus: EProcessStatus;
    token: string;
    authenticated: boolean;
}

export interface IUsersState {
    login: ILoginState;
}
