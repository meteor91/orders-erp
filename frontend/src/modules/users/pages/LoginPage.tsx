import * as React from 'react';
import {Layout, Row, Col} from 'antd';

import {LoginForm} from '../components/LoginForm';

const styles = {
    login: {height: '100vh'},
};

export class LoginPage extends React.Component<{}, {}> {
    render() {
        return (
            <Layout style={styles.login}>
                <Row type="flex" justify="space-around" align="middle" style={styles.login}>
                    <Col span={8}>
                        <LoginForm/>
                    </Col>
                </Row>
            </Layout>
        );
    }
}
