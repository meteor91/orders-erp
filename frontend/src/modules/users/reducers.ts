import {Action} from 'redux-actions';
import modify from 'core/utils/modify';
import {EProcessStatus} from 'core/Enums';
import {IUsersState} from './models';

export const initial = {
    get state(): IUsersState {
        const token = window.localStorage.getItem('token');
        return {
            login: {
                errors: null,
                token,
                status: EProcessStatus.IDLE,
                verifyStatus: token ? EProcessStatus.RUNNING : EProcessStatus.IDLE,
                authenticated: false,
            },
        };
    },
};

export function users(state: IUsersState = initial.state, action: Action<any>): IUsersState {
    const payload = action.payload;

    switch (action.type) {
        case 'users/login':
            return modify<IUsersState>(state).set('login', {
                token: null,
                status: EProcessStatus.RUNNING,
                authenticated: false,
            });

        case 'users/login_SUCCESS':
            return modify<IUsersState>(state).set('login', {
                token: payload.token,
                status: EProcessStatus.SUCCESS,
                authenticated: true,
            });

        case 'users/login_FAIL':
            return modify<IUsersState>(state).set('login', {
                errors: payload.errors,
                token: null,
                status: EProcessStatus.FAIL,
                authenticated: false,
            });
        case 'users/token/verify':
            return modify<IUsersState>(state).set('login', {
                token: payload.token,
                verifyStatus: EProcessStatus.RUNNING,
                authenticated: false,
            });
        case 'users/token/verify_SUCCESS':
            return modify<IUsersState>(state).set('login', {
                token: payload.token,
                verifyStatus: EProcessStatus.SUCCESS,
                authenticated: true,
            });
        case 'users/token/verify_FAIL':
            return modify<IUsersState>(state).set('login', {
                token: null,
                verifyStatus: EProcessStatus.FAIL,
                authenticated: false,
            });
    }
    return state;
}
